# system modules
import datetime
import glob
import itertools
import operator
import json
import logging
import os
import random
import unittest
from unittest.mock import patch

import numpy as np

# external modules
import pandas as pd

from sensemapi import paths

# internal modules
from sensemapi.account import SenseMapAccount
from sensemapi.cache import *
from sensemapi.errors import *
from sensemapi.senseBox import senseBox
from sensemapi.sensor import senseBoxSensor

from . import API_SERVER, needs_credentials
from .test_client import SenseMapClientUploadTest

# import logging
# logging.basicConfig(level = logging.DEBUG)


class SenseMapAccountBaseTest(unittest.TestCase):
    def setUp(self):
        self.account = SenseMapAccount(api=API_SERVER)

    def breakToken(self):
        self.account.token = self.account.token[5:] + self.account.token[:5]

    def breakRefreshToken(self):
        self.account.refresh_token = (
            self.account.refresh_token[5:] + self.account.refresh_token[:5]
        )

    def assertPropertiesNone(self):
        self.assertIsNone(self.account.email)
        self.assertIsNone(self.account.password)
        self.assertIsNone(self.account.username)
        self.assertIsNone(self.account.role)
        self.assertIsNone(self.account.language)
        self.assertIsNone(self.account.token)
        self.assertIsNone(self.account.refresh_token)
        self.assertEqual(len(self.account.boxes), 0)

    def assertPropertiesNotNone(self):
        self.assertIsNotNone(self.account.email)
        self.assertIsNotNone(self.account.password)
        self.assertIsNotNone(self.account.username)
        self.assertIsNotNone(self.account.role)
        self.assertIsNotNone(self.account.language)
        self.assertIsNotNone(self.account.token)
        self.assertIsNotNone(self.account.refresh_token)
        self.assertIsNotNone(self.account.boxes)


class SenseMapAccountObjectTest(SenseMapAccountBaseTest):
    def test_default_properties(self):
        self.account = SenseMapAccount(api=API_SERVER)
        self.assertIsNone(self.account.email)
        self.assertIsNone(self.account.password)
        self.assertIsNone(self.account.username)
        self.assertIsNone(self.account.role)
        self.assertIsNone(self.account.language)
        self.assertIsNone(self.account.token)
        self.assertIsNone(self.account.refresh_token)
        self.assertEqual(len(self.account.boxes), 0)

    def test_constructor_sets_properties(self):
        email = "test-email@internet.com"
        password = "test-email@internet.com"
        self.account = SenseMapAccount(
            email=email, password=password, api=API_SERVER
        )
        self.assertEqual(self.account.email, email)
        self.assertEqual(self.account.password, password)

    def test_read_account_details_correct(self):
        user_data = {
            "boxes": ["aefa63efa6543e65a3f65e36"],
            "email": "test-email@internet.com",
            "emailIsConfirmed": True,
            "language": "en_US",
            "name": "accountname",
            "role": "user",
        }
        self.assertTrue(self.account.read_user_details(user_data))
        self.assertEqual(self.account.email, user_data["email"])
        self.assertEqual(self.account.language, user_data["language"])
        self.assertEqual(self.account.username, user_data["name"])
        self.assertEqual(self.account.role, user_data["role"])
        self.assertEqual(len(self.account.boxes), 1)


class SenseMapAccountLoginTest(SenseMapAccountBaseTest):
    def setUp(self):
        super().setUp()
        self.account.email = os.environ.get("SENSEMAP_EMAIL")
        self.account.username = os.environ.get("SENSEMAP_USER")
        self.account.password = os.environ.get("SENSEMAP_PASSWORD")

    def tearDown(self):
        self.account.sign_out()
        self.assertIsNone(self.account.token)
        self.assertIsNone(self.account.refresh_token)

    def test_sign_in_without_email_or_username_raises(self):
        self.account.email = None
        self.account.username = None
        with self.assertRaises(NoUserError):
            self.account.sign_in()

    def test_sign_in_without_password_raises(self):
        self.account.password = None
        with self.assertRaises(NoCredentialsError):
            self.account.sign_in()

    def test_sign_in_with_invalid_credentials_fails(self):
        self.account.email = "bogus-email@nonexistant-domain.nope"
        self.account.password = "Nobody uses this string as password!"
        with self.assertRaises(OpenSenseMapAPIInvalidCredentialsError):
            self.account.sign_in()

    @needs_credentials
    def test_sign_in_sign_out(self):
        self.account.sign_in()
        self.assertPropertiesNotNone()

    @needs_credentials
    def test_refresh_tokens_bare(self):
        self.account.sign_in()
        t, r = self.account.token, self.account.refresh_token
        self.account._refresh_tokens()
        self.assertIsNotNone(self.account.token)
        self.assertIsNotNone(self.account.refresh_token)
        # assert that tokens changed
        self.assertNotEqual(t, self.account.token)
        self.assertNotEqual(r, self.account.refresh_token)

    @needs_credentials
    def test_refresh_tokens_bare_old_token_raises(self):
        self.account.sign_in()
        r = self.account.refresh_token
        self.breakRefreshToken()
        with self.assertRaises(OpenSenseMapAPIOutdatedTokenError):
            self.account._refresh_tokens()

    @needs_credentials
    def test_refresh_tokens_with_old_tokens_signs_in(self):
        self.account.sign_in()
        r = self.account.refresh_token
        self.breakRefreshToken()
        with patch.object(self.account, "sign_in") as mock:
            self.account.refresh_tokens()
        self.assertTrue(mock.called)

    @needs_credentials
    def test_authorization_header_getter_calls_sign_in_if_no_token(self):
        self.account.token = None
        with patch.object(self.account, "sign_in") as mock:
            self.account.authorization_header
        self.assertTrue(mock.called)

    @needs_credentials
    def test_authorization_header_getter_doesnt_call_sign_in_if_token(self):
        self.account.sign_in()
        self.assertIsNotNone(self.account.token)
        with patch.object(self.account, "sign_in") as mock:
            self.account.authorization_header
        self.assertFalse(mock.called)


@needs_credentials
class SenseMapAccountTest(SenseMapAccountBaseTest):
    def setUp(self):
        self.account = SenseMapAccount(api=API_SERVER)
        self.account.email = os.environ.get("SENSEMAP_EMAIL")
        self.account.username = os.environ.get("SENSEMAP_USER")
        self.account.password = os.environ.get("SENSEMAP_PASSWORD")
        self.account.sign_in()

    def tearDown(self):
        self.account.sign_out()

    def test_get_details(self):
        self.account.get_details()
        self.assertPropertiesNotNone()

    def test_get_details_old_tokens_refreshes_tokens(self):
        self.breakToken()
        t, r = self.account.token, self.account.refresh_token
        self.account.get_details()
        self.assertNotEqual(t, self.account.token)
        self.assertNotEqual(r, self.account.refresh_token)


@needs_credentials
class SenseMapAccountBoxManagementTest(SenseMapAccountBaseTest):
    def setUpClass():
        account = SenseMapAccount(api=API_SERVER)
        account.email = os.environ.get("SENSEMAP_EMAIL")
        account.username = os.environ.get("SENSEMAP_USER")
        account.password = os.environ.get("SENSEMAP_PASSWORD")
        box = senseBox(
            name="My senseBox",
            exposure="outdoor",
            description="Test senseBox",
            current_lat=48.52748,
            current_lon=9.06021,
        )
        box.new_sensor(
            title="temperature", unit="°C", type="DHT11", icon="osem-shock"
        )
        box.new_sensor(
            title="humidity", unit="%", type="DHT11", icon="osem-humidity"
        )
        new_box = account.new_box(box)
        unittest.TestCase().assertIsNotNone(new_box.id)
        unittest.TestCase().assertIsNotNone(
            box.id,
            "{}.new_box did not add id to box".format(
                account.__class__.__name__
            ),
        )
        unittest.TestCase().assertIsNotNone(
            box.description,
            "{}.new_box removes the description of handed senseBox".format(
                account.__class__.__name__
            ),
        )
        unittest.TestCase().assertIs(
            box.client,
            account,
            "{}.new_box(box) does not set itself to box.client".format(
                type(account).__name__
            ),
        )
        unittest.TestCase().assertIs(
            new_box.client,
            account,
            "Box returned by {}.new_box(box) does not have account set to "
            "box.client".format(type(account).__name__),
        )
        unittest.TestCase().assertEqual(new_box.id, box.id)

    def tearDownClass():
        # delete all boxes if these tests are over
        account = SenseMapAccount(api=API_SERVER)
        account.email = os.environ.get("SENSEMAP_EMAIL")
        account.username = os.environ.get("SENSEMAP_USER")
        account.password = os.environ.get("SENSEMAP_PASSWORD")
        for i in itertools.count(1):
            account.get_own_boxes()
            if len(account.boxes) == 0:
                break
            if i > 10:
                raise ValueError(
                    "For some reason, "
                    "after deleting all boxes {} times, "
                    "there are still {} boxes left: {}".format(
                        i,
                        len(account.boxes),
                        ", ".join(
                            map(
                                repr,
                                map(
                                    operator.attrgetter("name"), account.boxes
                                ),
                            )
                        ),
                    )
                )
            for box in account.boxes:
                account.delete_box(box.id, really=True)
        account.sign_out()

    def setUp(self):
        self.account = SenseMapAccount(api=API_SERVER)
        self.account.email = os.environ.get("SENSEMAP_EMAIL")
        self.account.username = os.environ.get("SENSEMAP_USER")
        self.account.password = os.environ.get("SENSEMAP_PASSWORD")
        self.account.get_own_boxes()
        self.assertGreater(
            len(self.account.boxes),
            0,
            "somehow, there are no boxes in the account...",
        )


class SenseMapAccountBoxMetadataChangeTest(SenseMapAccountBoxManagementTest):
    def test_upload_box_as_new(self):
        box = self.account.boxes[0]
        box.name = "Copy of '{}'".format(box.name)
        box.upload_as_new()
        self.assertIn(
            box.name,
            self.account.boxes.by_name,
            "Uploading senseBox as new did not work",
        )

    def test_get_own_boxes_fetches_coordinates(self):
        for box in self.account.boxes:
            self.assertIsNotNone(box.current_lat)
            self.assertIsNotNone(box.current_lon)

    def test_get_box_does_fetch_coordinates(self):
        for box in self.account.boxes:
            b = self.account.get_box(box.id)
            self.assertIsNotNone(b.current_lat)
            self.assertIsNotNone(b.current_lon)

    def test_change_box_metadata(self):
        for box in self.account.boxes:
            b = self.account.get_box(box.id)
            b.name = "Another name for the box"
            b.exposure = "indoor"
            b.current_lat = b.current_lat - 0.01
            b.current_lon = b.current_lon + 0.01
            b.upload_metadata()
            altered_box = self.account.get_box(b.id)
            self.assertEqual(altered_box, b)

    def test_delete_sensor(self):
        n_delete = 0
        n_last = 0
        for box in self.account.boxes:
            while True:
                L = len(box.sensors)
                if L > 1:
                    sensor = box.sensors[0]
                    sensor_id = sensor.id
                    sensor.delete(really=True)
                    self.assertFalse(
                        sensor_id in box.sensors.by_id,
                        "delete() did not delete the sensor",
                    )
                    self.assertEqual(
                        len(box.sensors),
                        L - 1,
                        "delete() did not reduce amount of sensors by 1",
                    )
                    n_delete += 1
                if L == 1:
                    with self.assertRaises(OpenSenseMapAPIError):
                        box.sensors[0].delete(really=True)
                    n_last += 1
                    break
                if L < 1:
                    self.fail("Box '{}' has {} sensors!?".format(box.id, L))
        self.assertGreater(
            n_delete, 0, "Could not test deletion of one of multiple sensors"
        )
        self.assertGreater(
            n_last, 0, "Could not attempt to delete the last sensor"
        )

    def test_get_box_adds_client_to_box(self):
        box = self.account.get_box(self.account.boxes[0].id)
        self.assertIs(box.client, self.account)


class SenseMapAccountMeasurementTest(SenseMapAccountBoxManagementTest):
    def test_sensor_measurement_upload_download(self):
        for box in self.account.boxes:
            sensor = box.sensors[0]
            if len(sensor.get_measurements()) > 0:
                sensor.delete_measurements(all=True, really=True)
            for i, val in enumerate((random.random(), 0)):
                with self.subTest(value=val):
                    sensor.last_time = None
                    sensor.last_value = val
                    sensor.upload_measurement()
                    mod_box = self.account.get_box(box.id)
                    self.assertEqual(mod_box.sensors[0].last_value, val)
                    data = sensor.get_measurements()
                    self.assertEqual(len(data), i + 1)
                    newest_val = data.data["value"][
                        max(
                            range(len(data.data["createdAt"])),
                            key=lambda i: data.data["createdAt"][i],
                        )
                    ]
                    self.assertTrue(np.allclose(newest_val, val))

    def test_sensor_measurement_delete_all(self):
        for box in self.account.boxes:
            sensor = box.sensors[0]
            sensor.last_time = None
            val = random.random()
            sensor.last_value = val
            sensor.upload_measurement()
            self.assertGreater(len(sensor.get_measurements()), 0)
            sensor.delete_measurements(all=True, really=True)
            self.assertEqual(len(sensor.get_measurements()), 0)

    def test_sensor_measurement_delete_upload_delete_multiple_only_value(self):
        for box in self.account.boxes:
            data = pd.DataFrame.from_dict(
                {
                    "sensor_id": list(box.sensors.by_id.keys()),
                    "value": [
                        random.random() for i in range(len(box.sensors))
                    ],
                }
            )
            self.account.post_measurements(
                box_id=box.id, measurements=data, discard_incomplete=True
            )
            box.fetch_metadata()
            for sensor in box.sensors:
                uploaded_value = data["value"][
                    data["sensor_id"] == sensor.id
                ].values[0]
                self.assertEqual(
                    sensor.last_value,
                    uploaded_value,
                    "Uploading value {} did not work, "
                    "current value is {}".format(
                        uploaded_value, sensor.last_value
                    ),
                )
