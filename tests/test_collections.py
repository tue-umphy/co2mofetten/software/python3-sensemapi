# system modules
import unittest

# internal modules
from sensemapi.senseBox import senseBox, senseBoxCollection
from sensemapi.sensor import senseBoxSensor, senseBoxSensorCollection

from .test_client import random_id

# external modules


class senseBoxCollectionTest(unittest.TestCase):
    def setUp(self):
        self.boxes = [
            senseBox(id=random_id(), name="SenseBox_{}".format(i))
            for i in range(5)
        ]
        self.collection = senseBoxCollection(self.boxes)

    def test_empty_by_name(self):
        self.assertEqual(senseBoxCollection().by_name, {})

    def test_by_name(self):
        for box in self.collection:
            self.assertIs(self.collection.by_name[box.name], box)

    def test_empty_by_id(self):
        self.assertEqual(senseBoxCollection().by_id, {})

    def test_by_id(self):
        for box in self.collection:
            self.assertIs(self.collection.by_id[box.id], box)

    def test_duplicated_by_name(self):
        for box in self.collection[2:4]:
            box.name = "Duplicated senseBox"
        for box in self.collection:
            self.assertIn(box, self.collection.by_name.values())

    def test_getattr(self):
        for box in self.collection:
            self.assertIs(getattr(self.collection, box.name), box)


class senseBoxSensorCollectionTest(unittest.TestCase):
    def setUp(self):
        self.sensors = [
            senseBoxSensor(id=random_id(), title="sensor_{}".format(i))
            for i in range(5)
        ]
        self.collection = senseBoxSensorCollection(self.sensors)

    def test_empty_by_title(self):
        self.assertEqual(senseBoxSensorCollection().by_title, {})

    def test_by_title(self):
        for sensor in self.collection:
            self.assertIs(self.collection.by_title[sensor.title], sensor)

    def test_empty_by_id(self):
        self.assertEqual(senseBoxSensorCollection().by_id, {})

    def test_by_id(self):
        for sensor in self.collection:
            self.assertIs(self.collection.by_id[sensor.id], sensor)

    def test_duplicated_by_title(self):
        for sensor in self.collection[2:4]:
            sensor.title = "Duplicated sensor"
        for sensor in self.collection:
            self.assertIn(sensor, self.collection.by_title.values())

    def test_getattr(self):
        for sensor in self.collection:
            self.assertIs(getattr(self.collection, sensor.title), sensor)
