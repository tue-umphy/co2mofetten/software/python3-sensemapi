# system modules
import os
import unittest
import logging

# internal modules
from sensemapi import paths

# external modules


API_SERVER = os.environ.get("SENSEMAPI_TEST_API", paths.OPENSENSEMAP_API_TEST)

if level := os.environ.get("SENSEMAPI_TEST_LOGLEVEL"):
    logging.basicConfig(level=level)


# decorator to skip tests that require credentials
def needs_credentials(f):
    return unittest.skipUnless(
        (os.environ.get("SENSEMAP_EMAIL") or os.environ.get("SENSEMAP_USER"))
        and os.environ.get("SENSEMAP_PASSWORD"),
        "no credentials specified via SENSEMAP_EMAIL/SENSEMAP_PASSWORD "
        "environment variables",
    )(f)
