# system modules
import unittest

# internal modules
from sensemapi.senseBox import senseBox

# external modules


class senseBoxBaseTest(unittest.TestCase):
    def setUp(self):
        box = senseBox()
        box = senseBox(
            name="My senseBox",
            exposure="outdoor",
            current_lat=48.52748,
            current_lon=9.06021,
        )
        box.new_sensor(
            title="temperature", unit="°C", type="DHT11", icon="osem-shock"
        )
        box.new_sensor(
            title="humidity", unit="%", type="DHT11", icon="osem-humidity"
        )
        self.box = box


class senseBoxJSONTest(senseBoxBaseTest):
    def test_empty_senseBox_to_api_json_from_api_json_consistency(self):
        box = senseBox()
        self.assertEqual(box, type(box).from_api_json(box.to_api_json()))

    def test_to_api_json_from_api_json_consistency(self):
        self.assertEqual(
            self.box, type(self.box).from_api_json(self.box.to_api_json())
        )
