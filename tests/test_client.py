# system modules
import logging
import os
import random
import textwrap
import unittest
from datetime import datetime, timezone, timedelta

import numpy as np
import pandas as pd

# external modules
import requests

from sensemapi import paths

# internal modules
from sensemapi.client import (
    SenseMapClient,
    senseBox,
    senseBoxCollection,
    senseBoxSensorData,
)

API_SERVER = os.environ.get("SENSEMAPI_TEST_API", paths.OPENSENSEMAP_API_TEST)


def random_id():
    return "".join(random.sample("abcdef0123456789" * 2, 25))


class SenseMapClientBaseTest(unittest.TestCase):
    def setUp(self):
        self.client = SenseMapClient(api=API_SERVER)
        self.client_live = SenseMapClient()


class SenseMapGetBoxesIntegrationTest(SenseMapClientBaseTest):
    """
    Class for Integration Tests that work directly with the API and try to
    examine wheter all Participants (API and SenseMapi) are functioning.
    """

    bbox_test_muenster = [7.562027, 51.932412, 7.684250, 51.987206]
    example_to_date = datetime(
        2018, 11, 4, 00, 00, 00, 000, tzinfo=timezone.utc
    )
    example_from_date = example_to_date - timedelta(days=1)

    def test_positive_bbox_get(self):
        """
        This Test does a simple check, wheter or not the Test-API can be
         reached and Boxes can be retrieved
        :return:
        """
        response = self.client.get_boxes(bbox=self.bbox_test_muenster)
        self.assertTrue(len(response) > 0)

    @unittest.expectedFailure
    def test_date_phenomenon_get(self):
        """
        This test tries to check, wheter or not the date and phenomenon
        settings work. Fixed date and fixed area, causing that these
        parameters (boxes_muenster_20191104) should never change.
        Running against Live-API, because Test-API doesn't
        list actively measuring boxes.
        """
        boxes_muenster_20191104 = 13
        # Check if API-Call is successfull and returns corer
        response = self.client_live.get_boxes(
            bbox=self.bbox_test_muenster,
            from_date=self.example_from_date,
            to_date=self.example_to_date,
            phenomenon=["PM10"],
        )

        self.assertEqual(len(response.boxes), boxes_muenster_20191104)

        for box in response.boxes:
            found_sensor = False
            for sensor in box.sensors:
                if sensor.title == "PM10":
                    found_sensor = True
            self.assertTrue(found_sensor)

    def test_grouptag_get(self):
        """
        This Test does a simple check, wheter or not the Test-API can be
        reached and Boxes with Grouptag can be filtered and retrieved.
        """
        example_grouptags = ["ifgi", "lehrerfortbildung"]
        # Test only one Grouptag
        response = self.client.get_boxes(
            bbox=self.bbox_test_muenster, grouptag=["Luftdaten"]
        )
        for box in response.boxes:
            self.assertEqual(box.grouptag, "Luftdaten")

        # Test multiple Grouptags
        response = self.client.get_boxes(
            bbox=self.bbox_test_muenster, grouptag=example_grouptags
        )
        for box in response.boxes:
            self.assertTrue(box.grouptag in example_grouptags)

    def test_minimal_get(self):
        """
        This Test does a simple check, wheter or not the Test-API can be
        reached and minimal Flag is working.
        Compare request with and without minimal flag (size-check)
        """
        response_minimal = self.client.get_boxes(
            bbox=self.bbox_test_muenster, minimal=True
        )

        for box in response_minimal.boxes:
            self.assertEqual(len(box.sensors), 0)
            self.assertEqual(box.grouptag, None)

    def test_exposure_get(self):
        """
        This Test does a simple check, wheter or not the Test-API
        can be reached and exposure filter can be set
        """
        exposures = ["outdoor"]
        response = self.client.get_boxes(
            bbox=self.bbox_test_muenster, exposure=exposures
        )
        for box in response.boxes:
            self.assertEqual(box.exposure, "outdoor")


class SenseMapGetBoxesUnitTest(SenseMapClientBaseTest):
    """
    Class for Unit-Tests that work only locally and
    try to break sensitive SenseMapi-Methods.
    """

    bbox_test_muenster = [7.562027, 51.932412, 7.684250, 51.987206]
    example_to_date = datetime(
        2018, 11, 4, 00, 00, 00, 000, tzinfo=timezone.utc
    )
    example_from_date = example_to_date - timedelta(days=1)

    def test_bbox(self):
        with self.assertRaises(AssertionError):
            self.client.get_boxes(from_date=self.example_to_date)

    def test_phenomenon_get(self):
        with self.assertRaises(ValueError):
            self.client_live.get_boxes(
                bbox=self.bbox_test_muenster, phenomenon=["PM10"]
            )


class SenseMapConnectionTest(SenseMapClientBaseTest):
    def test_connection_timeout(self):
        self.client.connection_timeout = 0.00001
        self.client.response_timeout = 0.00001
        with self.assertRaises(
            (requests.ReadTimeout, requests.ConnectTimeout)
        ):
            self.client.get_box(random_id())


class SenseMapClientUploadTest(SenseMapClientBaseTest):
    ids = [random_id() for i in range(5)]
    data = pd.DataFrame.from_dict(
        {
            "height": [7, 6, np.nan, 100, np.nan],
            "sensor_id": ids,
            "value": [3, 2, 1, 0, -1],
            "lat": [np.nan, 22, 33, 44, np.nan],
            "lon": [np.nan, np.nan, 33, 55, 66],
            "time": pd.date_range(
                start="2018-08-18", end="2018-08-20", periods=5
            ),
        }
    )
    data["time"].at[len(data.index) - 1] = pd.NaT
    csv = textwrap.dedent(
        """
        {3},0,2018-08-19T12:00:00Z,55.0,44.0,100.0
        {2},1,2018-08-19T00:00:00Z,33.0,33.0
        {0},3,2018-08-18T00:00:00Z
        {1},2,2018-08-18T12:00:00Z
        {4},-1
        """.format(
            *ids
        )
    ).lstrip()

    def test_dataframe_to_csv_for_upload_raises_at_duplicate_ids(self):
        data = self.data.copy()
        L = list(data["sensor_id"])
        data["sensor_id"] = L[:2] + L[:2] + L[1:2]
        with self.assertRaises(ValueError):
            self.client.dataframe_to_csv_for_upload(data)

    def test_dataframe_to_csv_for_upload(self):
        with self.assertLogs(
            logger=logging.getLogger("sensemapi.client"), level=logging.WARNING
        ) as logwarning:
            self.assertEqual(
                self.client.dataframe_to_csv_for_upload(
                    self.data, discard_incomplete=True
                ),
                self.csv,
            )
        self.assertTrue(any("incomplete" in log for log in logwarning.output))

    def test_dataframe_to_csv_for_upload_raises(self):
        with self.assertLogs(
            logger=logging.getLogger("sensemapi.client"), level=logging.WARNING
        ) as logwarning:
            with self.assertRaises(ValueError):
                self.client.dataframe_to_csv_for_upload(self.data)
        self.assertTrue(any("incomplete" in log for log in logwarning.output))
