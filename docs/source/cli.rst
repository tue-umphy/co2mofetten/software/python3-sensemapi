Command-Line Interface
======================

:mod:`sensemapi` comes with a :doc:`cli`.

Invocation
++++++++++

:mod:`sensemapi`'s :doc:`cli` is invoked from the command-line by launching the
``sensemapi`` command:

.. code-block:: sh

   sensemapi
   # Usage: sensemapi [OPTIONS] COMMAND [ARGS]...
   #
   #  Command-line OpenSenseMap access
   # ...

If running the above fails with something sounding like ``command not found:
sensemapi``, you may either always run ``python3 -m sensemapi`` instead of a
plain ``sensemapi`` or add the directory ``~/.local/bin`` to your ``PATH``
environment variable by appending the following line to your shell's
configuration (probabily ``.bashrc`` in your home directory):

.. code-block:: sh

   export PATH="$HOME/.local/bin:$PATH"

The :doc:`cli` is based on `Click`_ and requires a subcommand to be specified.
The available subcommands will be explained throughout this page.

Help
----

A help page is available for both the ``sensemapi`` command and each
subcommand. To access it, pass the ``--help`` (or ``-h``) option to it:

.. code-block:: sh

    sensemapi --help
    # Usage: sensemapi [OPTIONS] COMMAND [ARGS]...
    #
    #  Command-line OpenSenseMap access
    # ...
    sensemapi route_mqtt --help
    # Usage: sensemapi route_mqtt [OPTIONS]
    #
    #  Route data from an MQTT broker to the OpenSenseMap
    # ...


.. _cli logging:

Logging
-------

If something is not working as expected or the :doc:`cli` seems to ”hang”, you
might want to check some more verbose logging output to see what's going on.
You can increase the verbosity of the :doc:`cli` by passing the ``--verbose``
(or ``-v``) option one or multiple times to the ``sensemapi`` command:

.. code-block:: sh

   sensemapi -v ...
   # a little more output
   # ...
   sensemapi -vvvv ...
   # VERY much logging output
   # ...

Analogously, if you'd like less verbosity, specify the ``--quiet`` (or ``-q``)
options one or more times.

.. hint::

   Keep in mind that you have to specify the logging options **to the**
   ``sensemapi`` **command, BEFORE any subcommand**!

.. _cli download:

Downloading Data from the OpenSenseMap
++++++++++++++++++++++++++++++++++++++

.. code-block:: sh

    # Download data from all sensors of a given senseBox in a time frame
    sensemapi download --sensebox 5d35d315953683001a2b877a --from 2022-02 --to 2022-04 -o data.csv

    # Specify sensors
    sensemapi download --sensebox 5d35d315953683001a2b877a \
        --sensor 5d35d315953683001a2b877d \
        --sensor 5d35d315953683001a2b877e \
        -o sensor.csv

.. _cli routing mqtt:

Routing MQTT to the OpenSenseMap
++++++++++++++++++++++++++++++++


.. note::

    To enable this feature you need to install :mod:`sensemapi` like this:

    .. code-block:: sh

        python3 -m pip install --user 'sensemapi[mqtt]'

The `OpenSenseMap`_ can itself act as an `MQTT`_-client and subscribe to
specific topics on a broker (see for example - both links in German -
`ESP32-Sensordaten anzeigen auf openSenseMap`_  or the official docs
`Datenübertragung über MQTT`_).

However, the messages must contain the sensor IDs which can be annoying because
the publisher thus needs to know them. Hardcoding the IDs is a rather
unflexible approach and teaching all publishers how to interact with the
`openSenseMap API`_ to determine the sensor IDs automatically might be overkill
and not worth the effort (e.g. for microcontroller-based projects).

:mod:`sensemapi`'s :doc:`cli` offers a solution to this: the ``route_mqtt``
subcommand:

.. code-block:: sh

    sensemapi route_mqtt --help
    # Usage: sensemapi route_mqtt [OPTIONS]
    #
    #  Route data from an MQTT broker to the OpenSenseMap
    # ...

This subcommand connects both to the `openSenseMap API`_ (via a
:any:`SenseMapAccount`) and subscribes to topics of one or more MQTT brokers
(via `paho-mqtt`_). With user-defined rules the corresponding senseBox and
sensor name are extracted and composed from the topics and messages and the
data is uploaded to the `openSenseMap`_.

The matching mechanisms and login credentials are specified in a configuration
file which is then passed to the subcommand via the ``--config``  (or ``-c``)
option:

.. code-block:: sh

    sensemapi route_mqtt -c /path/to/config.conf

OpenSenseMap Access
-------------------

`openSenseMap`_ access configuration is placed in the ``[opensensemap]``
section:

.. code-block:: ini

   [opensensemap]
   # specify either email or username
   username = ...
   email = ...
   # your password
   password = ...
   # if you know what you're doing hostname and port may also be specified
   hostname = ...
   port = ...

.. hint::

   If you are not comfortable with your credentials lingering around in some
   plain-text file, you can also either use the ``--opensensemap`` option or
   set the ``SENSEMAPI_ROUTE_MQTT_OPENSENSEMAP`` environment variable to the
   url like

   .. code-block:: sh

      # set the environment variable ...
      export SENSEMAPI_ROUTE_MQTT_OPENSENSEMAP="username:password@api.opensensemap.org:port"
      sensemapi route_mqtt ...
      # ... or use the --opensensemap option
      sensemapi route_mqtt --opensensemap "username:password@api.opensensemap.org:port" ...


.. hint::

   If you are afraid that :mod:`sensemapi` might upload unwanted measurements
   to your precious senseBoxes, add the ``--dry-run`` option:

   .. code-block:: sh

       sensemapi -v route_mqtt --dry-run ...

   This will prevent anything from being uploaded to the OpenSenseMap. You can
   then examine the log messages and check if the right data would be uploaded
   to the right place. You might also want to turn verbose :ref:`cli logging`
   on.

MQTT Brokers
------------

Each MQTT broker to connect to is defined in its own ``[broker:some-title]``
section with fields that are described in the following. Generally, specifying
multiple values for a field can be done by specifying one value per line.

Connection Parameters
.....................

``hostname``, ``port``, ``ùsername`` and ``password`` are used for obvious
purposes, i.e. connecting to the broker.


``subscribe``
.............

This field contains the topics that the client should subscribe to. By default,
all topics (``#``) are subscribed to.

``parse_topic``
...............

This field contais regular expression patterns with `named capturing groups
<https://www.regular-expressions.info/named.html>`_ that are used to parse the
incoming topics. The matched values of the named capturing groups are then
available in the fields ``measurement``, ``sensebox_match`` and
``sensor_match`` . Incoming topics that are not matched by any regular
expression defined here are ignored. By default, the whole topic is captured
into a group named ``quantity``.

``parse_message``
.................

Analogously to ``parse_topic``, this field contains regular expressions to
parse incoming messages. By default, the first floating-point-number-looking
string is captured into a group named ``value``. The matched groups are merged
with the groups from ``parsed_topic``, whose values take precedence over
``parse_message``.

``measurement``
...............

This field contains one or more :any:`str.format`-templates where named
replacement fields are substituted with the values of the named capturing
groups from ``parse_topic`` and ``parse_message``. The first
:any:`str.format`-ed template that can be converted to :any:`float` is then
used as the measurement value to upload. The default is ``{value}``.

``replace``
...........

This field contains one or more replacement specifications (one by line) in the
format ``MATCH_REGEX = REPLACEMENT`` or ``MATCH_REGEX = REPLACEMENT --> KEY``.
Both ``MATCH_REGEX`` and ``REPLACEMENT`` are formatted with :any:`str.format`
as explained for the ``measurement`` field.  After formatting, ``MATCH_REGEX``
is interpreted as a regular expression that is attempted to match against all
named capturing groups from ``parse_topic`` and ``parse_message`` above. If
matched successfully, it will be replaced by ``REPLACEMENT``. If a ``KEY`` was
defined, instead of just replacing the values of the matching key of the named
capturing groups from above, that new key is created with the replaced string
and will be available in the following fields.  These replacements are
processed in order and may override each other.

This ``replace`` functionality is useful if your incoming MQTT topics don't
contain strings that exactly map to texts used in the OpenSenseMap, e.g. if you
use abbreviations or even different languages.

``sensebox_match``
..................

This field contains one or more :any:`str.format`-templates that are
:any:`str.format`-ed as explained for the ``measurement`` field. The results
are then interpreted as regular expressions to use for finding an appropriate
sensor to upload the data to. The regular expressions from this field are
matched against the senseBox metadata :any:`senseBox.name`,
:any:`senseBox.grouptag` and :any:`senseBox.description`.

``sensor_match``
................

Analogously to ``sensebox_match``, this field contains templated regular
expressions to match sensor metadata :any:`senseBoxSensor.title`,
:any:`senseBoxSensor.type` and :any:`senseBoxSensor.unit`.

``minimum_match``
.................


This field lists senseBox/sensor metadata field (senseBox ``name``,
``grouptag`` and ``description`` and  sensor ``title``, ``type`` and ``unit``)
combinations that should always be matched. For example, if you want to make
sure that only data is uploaded to the OpenSenseMap if the MQTT message
contains a unit, do the following:

- parse and capture the unit e.g. in a named capturing group ``unitstring`` in
  ``parse_topic`` or ``parse_message`` to make the ``unitstring`` key available
  in the ``sensor_match`` templates
- add someting like ``^{unitstring}$`` to ``sensor_match`` to match the unit as
  a whole string in the sensor metadata
- add (e.g. as a single line) ``unit`` to ``minimum_match`` to ensure that the
  :any:`senseBoxSensor.unit` metadata field is definitely matched with any
  pattern from ``sensor_match`` before uploading to the OpenSenseMap

The ``route_mqtt``-command tries hard to find the appropriate senseBox sensor
to upload the data to. It is first tries to match all senseBox/sensor metadata
with any of the respective appropriate patterns (``sensebox_match`` and
``sensor_match``). If this fails, the matching restrictions are reduced by
trying combinations of less metadata fields until a unique matching sensor is
found. If no matching sensor could be determined like this, there will be an
error log message. If there are too many matches, an error log message explains
it as well.

An example broker configuration section might look like this:

.. code-block:: ini

    [broker:localhost]

    # hostname = localhost # defaults to localhost anyway
    # port = 1883 # defaults to 1883 anyway
    # credentials (if needed)
    # username = ...
    # password = ...

    # topics to subscribe to
    # (MQTT topic subscription syntax)
    # topic structure here e.g. PLACE/[ROOM/]SENSOR/QUANTITY
    # but we are only interested in bme280 and scd30 indoor measurements
    # and data from our solar panel on the roof
    subscribe =
        indoor/+/bme280/+
        indoor/+/scd30/+
        roof/solarpanel/+

    # regular expressions (one per line) to parse the incoming topics
    # the first one parses the bme280 and scd30 measurements
    # the second one is for the solar panel
    parse_topic =
        ^(?P<place>indoor)/(?P<room>[^/]+)/(?P<sensor>bme280|scd30)/(?P<quantity>[^/]+)$
        ^(?P<place>roof)/(?P<sensor>solarpanel)/(?P<quantity>[^/]+)$
    # regular expressions (one per line) to parse the incoming message
    # our messages contain only the numeric measurement, so we match the whole
    # thing and assign it the name ”data”
    parse_message = ^(?P<data>[0-9.-]+)$

    # With the topic and message parsed, further information can be composed.
    # Except for minimum_match, all of the following fields are formatted with
    # Python's str.format-method and handed the merged named capturing groups
    # of the above matched regular expressions

    # what to use as the measurement
    # the result of the first formatted string that is interpretable as float is used
    measurement = {data}

    # On the OpenSenseMap, we didn't call our sensor the the solar panel data
    # just 'solarpanel' as it comes in via MQTT but 'Solar Panel' (with a
    # space). So in order for the matching to be successful, we create a new
    # key 'solar_panel' which we can then use below in the sensor_match field
    replace =
        ^solarpanel$ = Solar Panel --> solar_panel

    # what regular expression to use to find a matching senseBox
    # We have a senseBox called 'My Roof', 'The Bathroom' and 'The Kitchen'
    # The senseBox metadata 'name', 'grouptag' and 'description' is searched
    # for these patterns
    sensebox_match =
        My.*{place}
        The.*{room}
    # same as above but for the sensor
    # we look for a matching sensor e.g. named like 'temperature'
    # for topic indoor/bathroom/bme280/temperature
    # The sensors metadata 'title', 'type' and 'unit' is searched
    # for these patterns
    sensor_match =
        {sensor}
        {quantity}
        {solar_panel}

    # what combinations of senseBox/sensor metadata should at least be matched.
    # Available entries:
    # for the senseBox:
    #   - name, grouptag, description
    # for the sensor:
    #   - title, type, unit
    # You can specify multiple comma-separated combinations in multiple lines
    # Here, we just want the title to be definitely matched
    minimum_match =
        title





Now you can run the service:

.. code-block:: sh

   sensemapi route_mqtt -c /path/to/config.conf

You might also want to turn verbose :ref:`cli logging` on to see what exactly
is going on under the hood. Now as the MQTT messages arrive, they should be
parsed correctly and forwarded to the `openSenseMap`_.


Troubleshooting
---------------

If routing data from the MQTT brokers to the `openSenseMap`_ does not work as
expected, try the following:

- Turn verbose :ref:`cli logging` on to see what exactly is going on under the
  hood.
- If connecting to the `openSenseMap API`_ is the problem, check your
  credentials over at the `openSenseMap`_ web interface and from the examples
  in :ref:`Quickstart`.
- If connecting to the MQTT broker is the problem, check if you can connect
  with another client:

  .. code-block:: sh

     # for Debian-based systems:
     sudo apt-get install mosquitto-clients
     # see what's going on on the broker
     mosquitto_sub -v -h BROKER -t TOPIC_PATTERN
     # publish something to the broker manually
     mosquitto_pub -h BROKER -t TOPIC -m MESSAGE


.. _paho-mqtt: http://pypi.python.org/pypi/paho-mqtt
.. _Datenübertragung über MQTT: https://sensebox.github.io/books-v2/osem/mqtt_client.html
.. _ESP32-Sensordaten anzeigen auf openSenseMap: https://www.elektormagazine.de/news/esp32-sensordaten-anzeigen-auf-opensensemap
.. _MQTT: https://en.wikipedia.org/wiki/MQTT
.. _openSenseMap: https://opensensemap.org
.. _openSenseMap API: https://api.opensensemap.org
.. _openSenseMap API Documentation: https://docs.opensensemap.org
.. _Click: https://click.palletsprojects.com

